/* eslint-disable no-useless-escape */
const mongoose = require("mongoose");
const _= require("lodash");

const userSchema = mongoose.Schema({
  "username": {
    "type": "string",
    "unique": true,
    "minLength": 2,
    "maxLength": 16
  },
  "name": {
    "type": "string",
    "minLength": 1
  },
  "password": {
    "type": "string",
    "minLength": 4,
    "maxLength": 32
  },
  "email": {
    "type": "string",
    "unique": true
  },
  "bio": {
    "type": "string",
    "required": false
  },
  "image": {
    "type": "string",
    "required": false
  },
  "mailAddress": {
    "street1": {
      "type": "string",
      "required": false
    },
    "street2": {
      "type": "string",
      "required": false
    },
    "city": {
      "type": "string",
      "minLength": 3,
      "required": false
    },
    "state": {
      "type": "string",
      "minLength": 2,
      "maxLength": 2,
      "required": false
    },
    "zipcode": {
      "type": "string",
      "required": false
    }
  },
  "billAddress": {
    "street1": {
      "type": "string",
      "required": false
    },
    "street2": {
      "type": "string",
      "required": false
    },
    "city": {
      "type": "string",
      "minLength": 3,
      "required": false
    },
    "state": {
      "type": "string",
      "minLength": 2,
      "maxLength": 2,
      "required": false
    },
    "zipcode": {
      "type": "string",
      "required": false
    }
  },
  "phone": {
    "type": "string",
    "required": false
  },
  "emergencyContact": {
    "name": {
      "type": "string",
      "minLength": 1,
      "required": false
    },
    "phone": {
      "type": "string",
      "required": false
    }
  },
  "licenseNumber": {
    "type": "string",
    "required": false
  },
  "dateOfBirth": {
    "type": "Date"
  },
  "activeMember": {
    "type": "boolean"
  },
  "admin": {
    "type": "boolean"
  },
  "goodThrough": {
    "type": "Date"
  },
  "shareEmail": {
    "type": "boolean"
  },
  "createdAt": {
    "type": "Date"
  }
});

const validator = {
  username: {
    type: "string",
    min: 2,
    pattern: /^[a-zA-Z0-9]+$/
  },
  password: {
    type: "string",
    min: 8,
    max: 500
  },
  email: {
    type: "email"
  },
  bio: {
    type: "string",
    optional: true
  },
  image: {
    type: "string",
    optional: true
  },
  name: {
    type: "string",
    min: 1
  },
  mailAddress: {
    type: "object",
    optional: true,
    props: {
      street1: {
        type: "string"
      },
      street2: {
        type: "string",
        optional: true
      },
      city: {
        type: "string",
        min: 3
      },
      state: {
        type: "string",
        min: 2,
        max: 2
      },
      zipcode: {
        type: "string"
      }
    }
  },
  billAddress: {
    type: "object",
    optional: true,
    props: {
      street1: {
        type: "string"
      },
      street2: {
        type: "string",
        optional: true
      },
      city: {
        type: "string",
        min: 3
      },
      state: {
        type: "string",
        min: 2,
        max: 2
      },
      zipcode: {
        type: "string"
      }
    }
  },
  phone: {
    type: "string",
    optional: true
  },
  emergencyContact: {
    type: "object",
    optional: true,
    props: {
      name: {
        type: "string",
        min: 1
      },
      phone: {
        type: "string",
        pattern: /^\([0-9]{3}\)\ [0-9]{3}\-[0-9]{4}$/
      }
    }
  },
  licenseNumber: {
    type: "string",
    optional: true
  },
  dateOfBirth: {
    type: "date",
    convert: true
  },
  shareEmail: {
    type: "boolean"
  }
};

module.exports = {
  model: mongoose.model("users", userSchema),
  schema: userSchema,
  fields: ["_id","username","name", "email","bio","image","mailAddress","billAddress","phone","emergencyContact","licenseNumber","dateOfBirth","activeMember","goodThrough","admin","shareEmail","createdAt"],
  publicFields: ["_id","username","bio","name","image","email"],
  editableFields: ["name","email","bio","image","mailAddress","billAddress","phone","emergencyContact","licenseNumber","shareEmail"],
  validator: validator
};
