/* eslint-disable no-undef */
"use strict";

const path = require("path");
const mkdir = require("mkdirp").sync;
const DbService = require("moleculer-db");
const models = require("../models");

//rocess.env.MONGO_URI = "mongodb://localhost/smsms";

module.exports = function(name) {
  if (process.env.MONGO_URI) {
    // Mongo adapter
    const MongoAdapter = require("moleculer-db-adapter-mongoose");
    return {
      name: name,
      mixins: [DbService],
      adapter: new MongoAdapter(process.env.MONGO_URI),
      model: models("users").model
    };
  }

  // --- NeDB fallback DB adapter

  // Create data folder
  mkdir(path.resolve("./data"));

  return {
    mixins: [DbService],
    adapter: new DbService.MemoryAdapter({
      filename: `./data/smsms.db`
    })
  };
};
