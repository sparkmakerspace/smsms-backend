'use strict'

const { MoleculerClientError } = require('moleculer').Errors

const securePassword = require('secure-password')
const jwt = require('jsonwebtoken')
const _ = require('lodash')
const DbService = require('../mixins/db.mixin')
const userModel = require('../models/users')

const pwd = securePassword({ opslimit: 28 })

module.exports = {
  name: 'users',
  mixins: [DbService('users')],

  /**
   * Default settings
   */
  settings: {
    /** Secret for JWT */
    JWT_SECRET: process.env.JWT_SECRET || 'jwt-smsms-secret',

    fields: userModel.fields,

    entityValidator: userModel.validator,
  },

  /**
   * Actions
   */
  actions: {
    /**
     * Register a new user
     *
     * @actions
     * @param {Object} user - User entity
     *
     * @returns {Object} Created entity & token
     */
    create: {
      params: {
        user: {
          type: 'object',
        },
      },
      async handler(ctx) {
        let entity = ctx.params.user
        let found
        let age

        //
        // Input validation section
        //
        // Validate using the built-in validator
        entity = await this.validateEntity(entity)

        // Verify that the username isn't already registered
        if (entity.username)
          found = await this.adapter.findOne({
            username: entity.username,
          })
        if (found)
          return Promise.reject(
            new MoleculerClientError(
              'Username has already been registered!',
              422,
              '',
              [
                {
                  field: 'username',
                  message: 'has already been registered',
                },
              ]
            )
          )
        // Verify that the email isn't already registered
        if (entity.email)
          found = await this.adapter.findOne({
            email: entity.email,
          })
        if (found)
          return Promise.reject(
            new MoleculerClientError(
              'Email has already been registered!',
              422,
              '',
              [
                {
                  field: 'email',
                  message: 'has already been registered',
                },
              ]
            )
          )
        // Age checking!
        if (entity.dateOfBirth)
          entity.dateOfBirth = new Date(entity.dateOfBirth)
        age = await this.calculateAge(entity.dateOfBirth)
        if (age < 13)
          return Promise.reject(
            new MoleculerClientError(
              "Sorry, you're too young to register an account",
              422,
              '',
              [
                {
                  field: 'dateOfBirth',
                  message: 'Under the age of 13',
                },
              ]
            )
          )

        //
        // Private data infill section
        //
        // Now that we're sure that the user can at least register an account,
        // Hash the password and assign values to keys not exposed to user
        entity.password = pwd.hashSync(Buffer.from(entity.password))
        entity.bio = entity.bio || ''
        entity.image = entity.image || null
        entity.licenseNumber = entity.licenseNumber || null
        entity.admin = false
        entity.goodThrough = Date.now() - 1000 * 60 * 60 * 24 // yesterday just to be sure
        entity.activeMember = false
        entity.createdAt = new Date()

        //
        // Database and return section
        //
        // Insert the user entity and spit it back out to the client
        return this.adapter
          .insert(entity)
          .then(doc => this.transformDocuments(ctx, {}, doc))
          .then(user => this.transformEntity(user, true, ctx.meta.token))
          .then(json =>
            this.entityChanged('created', json, ctx).then(() => json)
          )
          .catch(err => console.log(err))
      },
    },

    /**
     * TODO: Account completion
     * Provides the fields that must be filled in for membership
     *
     */

    /**
     * Use Middleware that restricts results to just userModel.publicFields
     *
     */
    get: {
      auth: 'required',
      use: [
        async function middleware(ctx, next) {
          // dig into the context to get the authenticated user
          let user = await ctx.action.service.getById(ctx.meta.user._id)
          if (user) {
            if (user.admin === true || user.id === ctx.params.id)
              // if admin, don't restrict results, just call the handler
              return next()
            // otherwise, call the handler and then restrict the result in res to the publicFields
            return next().then(res => {
              // modifies the result
              return _.pick(res, userModel.publicFields)
            })
          }
          return Promise.reject(new MoleculerClientError('Not Authorized', 403))
        },
      ],
    },

    /**
     * Use Middleware that restricts results to just userModel.publicFields
     *
     */
    list: {
      auth: 'required',
      use: [
        async function middleware(ctx, next) {
          // dig into the context to get the authenticated user
          let user = await ctx.action.service.getById(ctx.meta.user._id)
          //console.log(user);
          if (user) {
            if (user.admin === true)
              // if admin, don't restrict results, just call the handler
              return next()
            // otherwise, call the handler and then restrict each result in res.rows to the publicFields
            return next().then(res => {
              // modifies the result
              res.rows = _.map(res.rows, row => {
                return _.pick(row, userModel.publicFields)
              })
              return res
            })
          }
          return Promise.reject(new MoleculerClientError('Not Authorized', 403))
        },
      ],
    },

    /**
     * Remove
     */
    remove: {
      auth: 'required',
      use: [
        async function middleware(ctx, next) {
          // dig into the context to get the authenticated user
          let user = await ctx.action.service.getById(ctx.meta.user._id)
          if (user)
            if (user.admin === true || user.id === ctx.params.id) {
              return next().then(() => {
                return { message: 'Account deleted' }
              })
            }
          return Promise.reject(
            new MoleculerClientError('Cannot delete other users', 403)
          )
        },
      ],
    },

    /**
     * Update
     */
    update: {
      auth: 'required',
      use: [
        async function middleware(ctx, next) {
          // dig into the context to get the authenticated user
          let user = await ctx.action.service.getById(ctx.meta.user._id)
          if (user)
            if (user.admin === true || user.id === ctx.params.id) {
              ctx.params = ctx.params.user
              return next()
            }
          return Promise.reject(
            new MoleculerClientError('Cannot edit other users', 403)
          )
        },
      ],
      async handler(ctx) {
        // user authentication is handled before handler is called.
        let entity
        //entity = await ctx.action.service.getById(ctx.params.id)
        return ctx.params.user
        this.adapter.updateById(ctx.params.id, entity)
      },
    },

    /**
     * Login with username & password
     *
     * @actions
     * @param {Object} user - User credentials
     *
     * @returns {Object} Logged in user with token
     */
    login: {
      params: {
        user: {
          type: 'object',
          props: {
            username: {
              type: 'string',
              minLength: 2,
            },
            password: {
              type: 'string',
              minLength: 8,
            },
          },
        },
      },
      handler(ctx) {
        const { username, password } = ctx.params.user

        return this.Promise.resolve()
          .then(() =>
            this.adapter.findOne({
              username,
            })
          )
          .then(user => {
            if (!user)
              return this.Promise.reject(
                new MoleculerClientError(
                  'Username or password is invalid!',
                  422,
                  '',
                  [
                    {
                      field: 'username',
                      message: 'Username not found',
                    },
                  ]
                )
              )

            let res = pwd.verifySync(
              Buffer.from(password),
              Buffer.from(user.password)
            )
            switch (res) {
              case securePassword.INVALID:
                return Promise.reject(
                  new MoleculerClientError(
                    'Username or password is invalid!',
                    422,
                    '',
                    [
                      {
                        field: 'password',
                        message: 'Wrong password',
                      },
                    ]
                  )
                )
              case securePassword.VALID_NEEDS_REHASH:
                user.password = pwd.hashSync(Buffer.from(password))
                this.adapter.insert(user)
                console.log('shit!')
              case securePassword.VALID:
                // Transform user entity (remove password and all protected fields)
                return this.transformDocuments(ctx, {}, user)
              default:
                return Promise.reject(
                  new MoleculerClientError(
                    'Username or password is invalid!',
                    422,
                    '',
                    [
                      {
                        field: 'password',
                        message: 'Wrong password',
                      },
                    ]
                  )
                )
            }
          })
          .then(user => this.transformEntity(user, true, ctx.meta.token))
      },
    },

    /**
     * Get user by JWT token (for API GW authentication)
     *
     * @actions
     * @param {String} token - JWT token
     *
     * @returns {Object} Resolved user
     */
    resolveToken: {
      cache: {
        keys: ['token'],
        ttl: 60 * 60, // 1 hour
      },
      params: {
        token: 'string',
      },
      handler(ctx) {
        return new this.Promise((resolve, reject) => {
          jwt.verify(
            ctx.params.token,
            this.settings.JWT_SECRET,
            (err, decoded) => {
              if (err) return reject(err)

              resolve(decoded)
            }
          )
        }).then(decoded => {
          if (decoded.id) return this.getById(decoded.id)
        })
      },
    },

    /**
     * Get current user entity.
     * Auth is required!
     *
     * @actions
     *
     * @returns {Object} User entity
     */
    me: {
      auth: 'required',
      cache: {
        keys: ['#token'],
      },
      handler(ctx) {
        return this.getById(ctx.meta.user._id)
          .then(user => {
            if (!user)
              return this.Promise.reject(
                new MoleculerClientError('User not found!', 400)
              )

            return this.transformDocuments(
              ctx,
              { fields: userModel.fields },
              user
            )
          })
          .then(user => this.transformEntity(user, true, ctx.meta.token))
      },
    },

    /**
     * Update current user entity.
     * Auth is required!
     *
     * @actions
     *
     * @param {Object} user - Modified fields
     * @returns {Object} User entity
     */
    updateMyself: {
      auth: 'required',
      params: {
        user: {
          type: 'object',
          props: {
            username: {
              type: 'string',
              minLength: 2,
              optional: true,
              pattern: /^[a-zA-Z0-9]+$/,
            },
            password: {
              type: 'string',
              minLength: 6,
              optional: true,
            },
            email: {
              type: 'email',
              optional: true,
            },
            bio: {
              type: 'string',
              optional: true,
            },
            image: {
              type: 'string',
              optional: true,
            },
          },
        },
      },
      handler(ctx) {
        const newData = ctx.params.user
        return this.Promise.resolve()
          .then(() => {
            if (newData.username)
              return this.adapter
                .findOne({
                  username: newData.username,
                })
                .then(found => {
                  if (
                    found &&
                    found._id.toString() !== ctx.meta.user._id.toString()
                  )
                    return Promise.reject(
                      new MoleculerClientError('Username is exist!', 422, '', [
                        {
                          field: 'username',
                          message: 'is exist',
                        },
                      ])
                    )
                })
          })
          .then(() => {
            if (newData.email)
              return this.adapter
                .findOne({
                  email: newData.email,
                })
                .then(found => {
                  if (
                    found &&
                    found._id.toString() !== ctx.meta.user._id.toString()
                  )
                    return Promise.reject(
                      new MoleculerClientError('Email is exist!', 422, '', [
                        {
                          field: 'email',
                          message: 'is exist',
                        },
                      ])
                    )
                })
          })
          .then(() => {
            newData.updatedAt = new Date()
            const update = {
              $set: newData,
            }
            return this.adapter.updateById(ctx.meta.user._id, update)
          })
          .then(doc => this.transformDocuments(ctx, {}, doc))
          .then(user => this.transformEntity(user, true, ctx.meta.token))
          .then(json =>
            this.entityChanged('updated', json, ctx).then(() => json)
          )
      },
    },

    /**
     * Get a user profile.
     *
     * @actions
     *
     * @param {String} username - Username
     * @returns {Object} User entity
     */
    profile: {
      cache: {
        keys: ['#token', 'username'],
      },
      params: {
        username: {
          type: 'string',
        },
      },
      handler(ctx) {
        return this.adapter
          .findOne({
            username: ctx.params.username,
          })
          .then(user => {
            if (!user)
              return this.Promise.reject(
                new MoleculerClientError('User not found!', 404)
              )

            return this.transformDocuments(ctx, {}, user)
          })
      },
    },
  },

  /**
   * Methods
   */
  methods: {
    /**
     * Generate a JWT token from user entity
     *
     * @param {Object} user
     */
    generateJWT(user) {
      const today = new Date()
      const exp = new Date(today)
      exp.setDate(today.getDate() + 60)

      return jwt.sign(
        {
          id: user._id,
          username: user.username,
          exp: Math.floor(exp.getTime() / 1000),
        },
        this.settings.JWT_SECRET
      )
    },

    /**
     * Transform returned user entity. Generate JWT token if neccessary.
     *
     * @param {Object} user
     * @param {Boolean} withToken
     */
    transformEntity(user, withToken, token) {
      if (user) {
        //user.image = user.image || "https://www.gravatar.com/avatar/" + crypto.createHash("md5").update(user.email).digest("hex") + "?d=robohash";
        user.image = user.image || ''
        if (withToken) user.token = token || this.generateJWT(user)
      }

      return {
        user,
      }
    },

    /**
     * Calculate the age of a user given their birthdate.
     * @param {Date} birthday
     *
     * @returns {Number} age
     */
    calculateAge(birthday) {
      // birthday is a date
      let ageDifMs = Date.now() - birthday.getTime()
      let ageDate = new Date(ageDifMs) // miliseconds from epoch
      return Math.abs(ageDate.getUTCFullYear() - 1970)
    },
  },

  events: {
    'cache.clean.users'() {
      if (this.broker.cacher) {
        this.broker.cacher.clean(`${this.name}.*`)
        console.log('user cache cleared')
      }
    },
  },
}
